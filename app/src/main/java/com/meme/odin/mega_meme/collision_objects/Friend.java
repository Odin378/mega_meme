package com.meme.odin.mega_meme.collision_objects;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.meme.odin.mega_meme.R;

import java.util.Random;

public class Friend extends Collision_Object{


    public Friend(Context context, int screenX, int screenY, int objectX, int objectY) {
        super(context,screenX,screenY,"friend",objectX,objectY);
        setRandomSpeed(10,16,false);
        Random generator = new Random();
        x = screenX;
        y = generator.nextInt(maxY) - bitmap.getHeight();
    }
}