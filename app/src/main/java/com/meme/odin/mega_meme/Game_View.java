package com.meme.odin.mega_meme;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.meme.odin.mega_meme.collision_objects.Collision_Object;
import com.meme.odin.mega_meme.collision_objects.Enemy;
import com.meme.odin.mega_meme.collision_objects.Friend;
import com.meme.odin.mega_meme.collision_objects.Player;
import com.meme.odin.mega_meme.collision_objects.Shot;
import com.meme.odin.mega_meme.collision_objects.Target;

import java.util.ArrayList;

public class Game_View extends SurfaceView implements Runnable {

    volatile boolean playing;
    private Thread gameThread = null;

    //Collision Objects
    private Player player;

    private Target target;
    private ArrayList<Shot> shots;

    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;


    //a screenX holder
    int screenX;

    //indicator that the enemy has just entered the game screen
    boolean flag;

    //an indicator if the game is over
    private boolean isGameOver;

    public Game_View(Context context, int screenX, int screenY) {
        super(context);
        player = new Player(context, screenX, screenY,(int)((double)screenX/8),(int)((double)screenY/8));

        surfaceHolder = getHolder();
        paint = new Paint();

        //Target
//        target = new Target(context, screenX, screenY,300, 100);

//        shots = new ArrayList<Shot>();

        this.screenX = screenX;

        isGameOver = false;
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
            control();
        }
    }

    private void update() {
//        ArrayList<Collision_Object> collisionObjects = getGameCollisionObjects();

        player.update();
    }

//    private ArrayList<Collision_Object> getGameCollisionObjects(){
//        ArrayList<Collision_Object> collisionObjects = new ArrayList<Collision_Object>();
//
//        return collisionObjects;
//    }

    //<editor-fold desc="Drawing Functions">
    private void draw() {
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.BLACK);


            paint.setColor(Color.WHITE);
            paint.setTextSize(20);

            //drawing the score on the game screen
            paint.setTextSize(30);

            canvas.drawBitmap(
                    player.getBitmap(),
                    player.getX(),
                    player.getY(),
                    paint);


            //draw game Over when the game is over
            if(isGameOver){
                this.drawGameOver();
            }

            surfaceHolder.unlockCanvasAndPost(canvas);

        }
    }

    private void drawGameOver(){
        paint.setTextSize(150);
        paint.setTextAlign(Paint.Align.CENTER);

        int yPos=(int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
        canvas.drawText("Game Over",canvas.getWidth()/2,yPos,paint);
    }
    //</editor-fold>

    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                player.swapBitmap();
                float rawX = motionEvent.getRawX();
                float rawY = motionEvent.getRawY();
                break;
        }
        return true;
    }
}