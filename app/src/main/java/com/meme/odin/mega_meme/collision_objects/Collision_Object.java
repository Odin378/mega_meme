package com.meme.odin.mega_meme.collision_objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.meme.odin.mega_meme.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public abstract class Collision_Object {

    //<editor-fold desc="Properties">

    protected Bitmap bitmap;

    //<editor-fold desc="Position">
    protected int x;
    protected int y;
    //</editor-fold>

    //<editor-fold desc="Screen Info">
    protected int maxY;
    protected int minY;

    protected int maxX;
    protected int minX;
    //</editor-fold>

    //<editor-fold desc="Speed">
    protected int speedX;
    protected int speedY;

    protected Context mainContext;


    protected static final int gravity = -10;
    //</editor-fold>


    protected final String resourceLocation = "com.meme.odin.mega_meme";

    //<editor-fold desc="Hit Boxes">
    protected ArrayList<Rect> hitBoxes;
    protected float[][] hitBoxConfig = {{0,0,0,0},{.1f,.1f,.1f,.1f}};
    //</editor-fold>
    //</editor-fold>

    //<editor-fold desc="Constructor">
    //Create the collision object, set the position, set the bitmap, then set the hit boxes
    public Collision_Object(Context context, int screenX, int screenY, String bitmapName, int objectX, int objectY) {
        x = objectX;
        y = objectY;
        String bitmapLocation = this.resourceLocation + ":drawable/" + bitmapName;
        int resourceId = context.getResources().getIdentifier(bitmapLocation,null,null);
        bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);

        mainContext = context;
        maxX = screenX;
        minX = 0;

        maxY = screenY - bitmap.getHeight();
        minY = 0;

        hitBoxes = new ArrayList<Rect>();

        updateHitBoxes();
    }
    //</editor-fold>

    //<editor-fold desc="Update Methods">
    //Update both positions and hit boxes
    public void update() {
        updatePosition();
        updateHitBoxes();
    }

    //Update the position based on speed
    protected void updatePosition(){
        int newX = x + speedX;
        int newY = y + (speedY + gravity);
        ArrayList<Rect> newHitBoxes = getHitBoxes(newX,newY);

    }

    //Get new hit boxes
    protected ArrayList<Rect> getHitBoxes(int newX, int newY){
        ArrayList<Rect> newHitBoxes = new ArrayList<Rect>();
        //Make new hit boxes
        for(float[] hitBoxInfo : hitBoxConfig){
            int left = (int)((float) newX * hitBoxInfo[0]);
            int top = (int)((float) newY * hitBoxInfo[0]);
            int right = (int)((float) bitmap.getWidth() * hitBoxInfo[0]);
            int bottom = (int)((float) bitmap.getHeight() * hitBoxInfo[0]);
            Rect hitBox = new Rect(left,top,right,bottom);
            newHitBoxes.add(hitBox);
        }

        return newHitBoxes;
    }

    protected void updateBitmap(String bitmapName,int scaleX, int scaleY){
        String bitmapLocation = this.resourceLocation + ":drawable/" + bitmapName;
        int resourceId = mainContext.getResources().getIdentifier(bitmapLocation,null,null);
        bitmap = BitmapFactory.decodeResource(mainContext.getResources(), resourceId);
        System.out.println(bitmapName);
        if(scaleX != 0 || scaleY != 0){
            bitmap = Bitmap.createScaledBitmap(bitmap,scaleX,scaleY,false);
        }

    }

    //Function to update the hitboxes for things
    protected void updateHitBoxes(){
        //Clear old hit boxes
        hitBoxes.clear();
        //Make new hitboxes
        for(float[] hitBoxInfo : hitBoxConfig){
            int left = (int)((float) x * hitBoxInfo[0]);
            int top = (int)((float) y * hitBoxInfo[0]);
            int right = (int)((float) bitmap.getWidth() * hitBoxInfo[0]);
            int bottom = (int)((float) bitmap.getHeight() * hitBoxInfo[0]);
            Rect hitBox = new Rect(left,top,right,bottom);
            hitBoxes.add(hitBox);
        }
    }
    //</editor-fold>


    //<editor-fold desc="Getters and setters">
    //one more getter for getting the rect object
    public ArrayList<Rect> getHitBoxes() {
        return hitBoxes;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() { return speedY;}

    //Todo: Setters maybe, would prefer methods in class to focus on those things
    public void setRandomSpeed(int lowerBound, int upperBound, boolean vertical){
        Random RNG = new Random();
        int highestRand = upperBound - lowerBound;
        int randomInt = RNG.nextInt(highestRand) + lowerBound;
        if(vertical){
            speedY = randomInt;
        }
        else{
            speedX = randomInt;
        }
    }
    //</editor-fold>

    //<editor-fold desc="Static Methods">
    //Checks 2 ArrayList<Rect>
    public static boolean checkHitBoxes(ArrayList<Rect> hitBoxes1, ArrayList<Rect> hitBoxes2){
        for(int i = 0;i < hitBoxes1.size(); i++){
            Rect rect1 = hitBoxes1.get(i);
            for(int j = 0; j < hitBoxes2.size();j++){
                Rect rect2 = hitBoxes2.get(j);

                //Check if the rectangles intersect, if so return true
                boolean collide = Rect.intersects(rect1,rect2);
                if(collide){
                    return true;
                }
            }
        }
        //Hit boxes did not collide
        return false;
    }
    //</editor-fold>
}
