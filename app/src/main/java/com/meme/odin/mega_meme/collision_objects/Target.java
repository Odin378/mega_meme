package com.meme.odin.mega_meme.collision_objects;

import android.content.Context;

import java.util.Random;

public class Target extends Collision_Object {

    protected float[][] hitBoxConfig = {{1f,1f,1f,1f}};

    public Target(Context context, int screenX, int screenY, int objectX, int objectY) {
        super(context, screenX, screenY, "target", objectX, objectY);
        setRandomSpeed(10,20,false);
        setRandomSpeed(5,20,true);
    }

    protected void updatePosition(){

    }
}
