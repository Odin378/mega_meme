package com.meme.odin.mega_meme.collision_objects;

import android.content.Context;

public class Shot extends Collision_Object {

    public Shot(Context context, int screenX, int screenY, String bitmapName, int objectX, int objectY, int angle, int power) {
        super(context, screenX, screenY, bitmapName, objectX, objectY);
        setSpeeds(angle, power);
    }

    //Set the initial speed
    protected void setSpeeds(int angle, int power){
        double cos = Math.cos((double) angle);
        double sin = Math.sin((double) angle);
        speedX = (int)((double)power * cos);
        //-1 because 0,0 is in top left corner
        speedY = -1 * (int)((double)power * sin);
    }
}
