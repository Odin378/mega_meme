package com.meme.odin.mega_meme.collision_objects;

import android.content.Context;

public class Terrain extends Collision_Object {

    public Terrain(Context context, int screenX, int screenY, String bitmapName, int objectX, int objectY) {
        super(context, screenX, screenY, bitmapName, objectX, objectY);
        speedX = 0;
        speedY = 0;
    }
}
