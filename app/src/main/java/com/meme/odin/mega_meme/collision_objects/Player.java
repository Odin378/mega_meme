package com.meme.odin.mega_meme.collision_objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.meme.odin.mega_meme.R;

/**
 * Created by Belal on 6/24/2016.
 */
public class Player extends Collision_Object{


    protected float[][] detectCollision = {{1f,1f,1f,1f}};

    protected boolean feelsBad = false;

    public Player(Context context, int screenX, int screenY, int objectX, int objectY) {
        super(context, screenX, screenY, "feelsgoodman", objectX, objectY);
        bitmap = Bitmap.createScaledBitmap(bitmap,200,150,false);
    }

    public void update() {
        super.update();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void swapBitmap(){
        if(feelsBad){
            feelsBad = false;
            this.updateBitmap("feelsgoodman",200,150);
        }
        else{
            feelsBad = true;
            this.updateBitmap("feelsbadman",200,150);
        }
    }

}